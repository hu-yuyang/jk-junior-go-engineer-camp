//go:build wireinject

package wire

import (
	"JK-Junior-Go-Engineer-Camp/wire/repository"
	"JK-Junior-Go-Engineer-Camp/wire/repository/dao"
	"github.com/google/wire"
)

func InitUserRepository() *repository.UserRepository {
	wire.Build(repository.NewUserRepository, dao.NewUserDAO, InitDB)
	// 不要 return nil
	return &repository.UserRepository{}
}
