package main

// DeferClosureV1 作为参数传入
func DeferClosureV1() {
	j := 0
	//defer func(j int) {
	//	println(j) // 0
	//}(j)
	defer func(val int) {
		println(val) // 0
	}(j)
	j = 1
}

// DeferClosureV2 作为闭包上下文传入
func DeferClosureV2() {
	i := 0
	defer func() {
		println(i) // 1
	}()
	i = 1
}

// DeferReturnV1 匿名返回值
func DeferReturnV1() int {
	a := 0
	defer func() {
		a = 1
	}()
	return a
}

// DeferReturnV2 命名返回值
func DeferReturnV2() (a int) {
	a = 0
	defer func() {
		a = 1
	}()
	return a
}

// MyStruct 返回值为地址
type MyStruct struct{ Name string }

func DeferReturnV3() *MyStruct {
	a := &MyStruct{Name: "Jerry"}
	defer func() {
		a.Name = "Tom"
	}()
	return a
}
