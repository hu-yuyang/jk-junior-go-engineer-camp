package main

type user struct {
	name string
}

// ForRange 使用for-range易错点
func ForRange() {
	users := []user{
		{name: "Tom"},
		{name: "Jerry"},
	}
	m := make(map[string]*user)
	for _, val := range users {
		m[val.name] = &val
	}
	for k, v := range m {
		println(k, (*v).name)
	}
}
