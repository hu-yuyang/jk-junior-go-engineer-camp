package main

import (
	"fmt"
	"sync/atomic"
)

func main() {
	var val int32 = 1
	// 原子读，你不会读到修改到一半的数据
	val = atomic.LoadInt32(&val)
	// 原子写，即便别的 Goroutine 在别的 CPU 上，也可以立即看到
	atomic.StoreInt32(&val, 1)
	// 原子自增，返回自增后的结果
	atomic.AddInt32(&val, 2)
	var newVal int32 = 10
	// CAS 操作
	// 如果地址上的值是old的值，就交换，否则
	atomic.CompareAndSwapInt32(&val, 3, newVal)
	fmt.Println(val, newVal)
}
