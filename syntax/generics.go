package main

import "fmt"

//type List interface {
//	// Add 指定位置添加一个节点
//	Add(idx int, val any)
//	// Append 追加一个节点
//	Append(val any)
//	// Delete 删除指定节点
//	Delete(idx int)
//}

type ListV1[T any] interface {
	// Add 指定位置添加一个节点
	Add(idx int, val T)
	// Append 追加一个节点
	Append(val T)
	// Delete 删除指定节点
	Delete(idx int)
}

// 泛型结构体
type nodeV1[T any] struct {
	data T
}

type LinkedListV1[T any] struct {
	head *nodeV1[T]
}

// Append 只有在声明T的地方才需要加 约束
func (ll LinkedListV1[T]) Append(val T) {}

func UseList() {
	ll := LinkedListV1[int]{}
	ll.Append(123)
	//ll.Append("123") // 会报错，这样就达到了约束类型的效果
}

type Number interface {
	// 这种定义方式就是声明一种约束，
	// 约束他为 int 或 int的衍生类型 或 uint类型
	~int | uint
}

func Sum[T Number](vals []T) T {
	var res T
	for _, val := range vals {
		// 这里会报错，说你不能在T类型上使用+，
		res = res + val
	}
	return res
}

func Max[T Number](vals []T) T {
	max := vals[0]
	for _, val := range vals {
		if max < val {
			max = val
		}
	}
	return max
}

// Find 找到filter返回true的元素
func Find[T Number](vals []T, filter func(t T) bool) T {
	for _, val := range vals {
		if filter(val) {
			return val
		}
	}
	var t T
	return t
}

// SliceInsert 插入
func SliceInsert[T Number](idx int, val T, vals []T) {
	// 先放到最后
	vals = append(vals, val)
	for i := len(vals) - 1; i > idx; i-- {
		vals[i-1], vals[i] = vals[i], vals[i-1]
	}
	fmt.Println(vals)
}
