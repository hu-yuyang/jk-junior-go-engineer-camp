package main

import "fmt"

type Fish interface {
	Swim()
}

// BigFish 实现了Fish 接口
type BigFish struct{}

func (f BigFish) Swim() {}

// BigFishAlias BigFish的别名，别名除了名字不同外，没有任何区别
// 这里用Goland可以看到也是实现了Fish接口的
type BigFishAlias = BigFish

// FakeFish BigFish的衍生类型，并没有实现Fish接口
type FakeFish BigFish

func (f FakeFish) FakeSwim() {}

func UseFish() {
	f1 := BigFish{}
	f2 := FakeFish{}
	// 衍生类型之间可以互相转换
	f3 := BigFish(f2)
	f4 := FakeFish(f1)
	fmt.Println(f3, f4)
}
