package main

import "fmt"

func MapStudy() {
	m1 := make(map[string]string, 4)
	m1["k1"] = "v1"
	m1["k2"] = "v2"
	m1["k3"] = "v3"
	m1["k4"] = "v4"
	m1["k5"] = "v5"
	fmt.Print("第一次遍历：") // k1 k2 k3 k4 k5
	for key, _ := range m1 {
		fmt.Print(key, " ")
	}
	fmt.Println()
	fmt.Print("第二次遍历：") // k4 k5 k1 k2 k3
	for key, _ := range m1 {
		fmt.Print(key, " ")
	}
}
