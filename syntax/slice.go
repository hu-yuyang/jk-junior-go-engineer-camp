package main

import "fmt"

func SubSlice() {
	s1 := []int{1, 2, 3, 4, 5}
	s2 := s1[1:3]                                            // [2,3]
	fmt.Printf("len(s2)=%d, cap(s2)=%d\n", len(s2), cap(s2)) // len(s2)=2, cap(s2)=4
	s2[0] = 6
	fmt.Printf("修改s2后的s1: %v\n", s1) // [1 6 3 4 5]
	s2[0] = 2
	s1 = append(s1, 6)                                             // s1 发生扩容
	fmt.Printf("s1扩容后 len(s1)=%d, cap(s1)=%d\n", len(s1), cap(s1)) // len(s1)=6, cap(s1)=10
	s2[0] = 7
	fmt.Printf("修改s2后的s1: %v\n", s1) // [1 2 3 4 5 6]
}
