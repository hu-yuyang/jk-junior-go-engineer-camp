package main

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
	"log"
	"time"
)

func main() {
	initViper()
	server := InitWebServer()
	if err := server.Run(":8081"); err != nil {
		panic(err)
	}
}

func initViper() {
	viper.SetConfigName("dev")    // 读取的文件名叫做 dev
	viper.SetConfigType("yaml")   // 读取的类型是yaml文件
	viper.AddConfigPath("config") // 文件位置在当前目录的 config 目录下
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func initViperV1() {
	cFilePath := pflag.String("config",
		"config/config.yaml", "配置文件路径")
	pflag.Parse()
	viper.SetConfigFile(*cFilePath)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
	fmt.Println(viper.GetString("db.dsn"))
}

func initViperV1Watch() {
	cFilePath := pflag.String("config",
		"config/config.yaml", "配置文件路径")
	pflag.Parse()
	viper.SetConfigFile(*cFilePath)
	viper.WatchConfig()
	// 监听配置文件变化
	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Println(viper.GetString("test.key"))
	})
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func initViperV2Remote() {
	err := viper.AddRemoteProvider("etcd3",
		"http://127.0.0.1:12379", "webook")
	if err != nil {
		panic(err)
	}
	viper.SetConfigType("yaml")

	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Println("远程配置中心发送变更")
	})
	err = viper.ReadRemoteConfig()
	if err != nil {
		panic(err)
	}
	go func() {
		for {
			err = viper.WatchRemoteConfig()
			if err != nil {
				panic(err)
			}
			log.Println(viper.GetString("test.key"))
			time.Sleep(time.Second * 3)
		}
	}()
}
