package ioc

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/service/sms"
	"JK-Junior-Go-Engineer-Camp/webook/internal/service/sms/localsms"
	"JK-Junior-Go-Engineer-Camp/webook/internal/service/sms/tencent"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
	tencentSMS "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/sms/v20210111"
	"os"
)

// InitSMSService 初始化短信服务
func InitSMSService() sms.Service {
	return initSMSMemoryService()
	//return initSMSTencentService()
	// return ratelimit.NewRateLimitSMSService(initSMSMemoryService(), limiter.NewRedisSlidingWindowLimiter())
}

// 初始化本地的短信服务（打印到控制台，不发短信）
func initSMSMemoryService() sms.Service {
	return localsms.NewService()
}

// 初始化腾讯云的短信服务
func initSMSTencentService() sms.Service {
	// 这些都在环境变量中，需要手动设置
	secretId, ok := os.LookupEnv("SMS_SECRET_ID")
	if !ok {
		panic("未找到环境变量 SMS_SECRET_ID")
	}
	secretKey, ok := os.LookupEnv("SMS_SECRET_KEY")
	if !ok {
		panic("未找到环境变量 SMS_SECRET_KEY")
	}

	// 获取 腾讯云 SMS 客户端
	client, err := tencentSMS.NewClient(
		// 鉴权
		common.NewCredential(secretId, secretKey),
		// 地区
		"ap-nanjing",
		// 我们这里没有什么定制化的东西，所以直接 New一个就Ok了
		profile.NewClientProfile(),
	)
	if err != nil {
		panic(err)
	}
	return tencent.NewService(client, "1400842696", "妙影科技")
}
