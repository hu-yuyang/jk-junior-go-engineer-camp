package ioc

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/service/oauth2/wechat"
	"JK-Junior-Go-Engineer-Camp/webook/pkg/logger"
)

func InitWechatService(l logger.LoggerV1) wechat.Service {
	// todo: appid属于敏感信息，这里暂时放到环境变量中，后面需要修改，应该放到配置文件中
	//appId, ok := os.LookupEnv("WECHAT_APP_ID")
	//if !ok { // 没有这个环境变量
	//	panic("未找到环境变量 WECHAT_APP_ID")
	//}
	//appSecret, ok := os.LookupEnv("WECHAT_APP_SECRET")
	//if !ok { // 没有这个环境变量
	//	panic("未找到环境变量 WECHAT_APP_SECRET")
	//}
	//return wechat.NewService(appId, appSecret)
	return wechat.NewService("appId", "appSecret", l)
}
