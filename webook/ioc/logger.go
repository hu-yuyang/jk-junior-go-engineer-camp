package ioc

import (
	"JK-Junior-Go-Engineer-Camp/webook/pkg/logger"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

// InitLogger 初始化日志模块
func InitLogger() logger.LoggerV1 {
	cfg := zap.NewDevelopmentConfig()
	//cfg := zap.NewProductionConfig()
	err := viper.UnmarshalKey("log", &cfg)
	if err != nil {
		panic(err)
	}
	l, err := cfg.Build()
	if err != nil {
		panic(err)
	}
	return logger.NewZapLogger(l)
}
