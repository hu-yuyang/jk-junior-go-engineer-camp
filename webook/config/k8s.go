//go:build k8s

// Package config K8s环境下的配置文件
package config

var Config = config{
	DB: DBConfig{
		// 注意这里就不能像本地配置的写法那样了
		//DSN: "root:root@tcp(localhost:13306)/webook?loc=Local&parseTime=True",
		// 因为此时 后端应用 和 MySQL 都是运行在k8s里的，
		// 所以直接使用 mysql-service里的 name 和 port 即可。
		DSN: "root:root@tcp(webook-mysql:3308)/webook?loc=Local&parseTime=True",
	},
	Redis: RedisConfig{
		// Redis 同理
		Addr: "webook-redis:6380",
	},
}
