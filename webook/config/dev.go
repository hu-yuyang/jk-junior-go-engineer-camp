//go:build !k8s

// Package config 不使用K8s的情况使用的配置文件
package config

var Config = config{
	DB: DBConfig{
		DSN: "root:root@tcp(localhost:13316)/webook?loc=Local&parseTime=True",
	},
	Redis: RedisConfig{
		Addr: "localhost:16379",
	},
}
