package config

// 配置映射的结构体
type config struct {
	DB    DBConfig
	Redis RedisConfig
}

// DBConfig MySQL配置
type DBConfig struct {
	DSN string
}

// RedisConfig Redis配置
type RedisConfig struct {
	Addr string
}
