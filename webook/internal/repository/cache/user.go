package cache

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/domain"
	"context"
	"encoding/json"
	"fmt"
	"github.com/redis/go-redis/v9"
	"time"
)

var ErrKeyNotExist = redis.Nil

// UserCache 用户业务专属缓存
type UserCache interface {
	Get(ctx context.Context, uid int64) (domain.User, error)
	Set(ctx context.Context, domainUser domain.User) error
}

// RedisUserCache 用户业务专属缓存-Redis实现
type RedisUserCache struct {
	cmd        redis.Cmdable
	expiration time.Duration // 过期时间
}

func NewRedisUserCache(cmd redis.Cmdable) UserCache {
	return &RedisUserCache{
		cmd: cmd,
		// 这里直接写死，我们这个缓存是专属于User业务的，
		// 用户缓存时间基本不会存在修改的情况
		// 如果是通用的那种，就需要作为参数传入了
		expiration: time.Minute * 15,
	}
}

// Get 根据用户ID 获取缓存中的用户信息
func (c *RedisUserCache) Get(ctx context.Context, uid int64) (domain.User, error) {
	key := c.key(uid)
	// 我们这里假定data为json格式
	data, err := c.cmd.Get(ctx, key).Result()
	if err != nil {
		return domain.User{}, err
	}
	var domainUser domain.User
	err = json.Unmarshal([]byte(data), &domainUser)
	// 直接返回即可，如果他有err，你的domainUser就是空的，所以这里没必要进行错误判断
	return domainUser, err
}

// Set 缓存用户信息
func (c *RedisUserCache) Set(ctx context.Context, domainUser domain.User) error {
	key := c.key(domainUser.Id)
	jsonData, err := json.Marshal(&domainUser)
	if err != nil {
		return err
	}
	return c.cmd.Set(ctx, key, &jsonData, c.expiration).Err()
}

// 根据用户 uid 获取指定格式的 key
func (c *RedisUserCache) key(uid int64) string {
	// 这个格式的规范没啥硬性要求
	return fmt.Sprintf("user:info:%d", uid)
}
