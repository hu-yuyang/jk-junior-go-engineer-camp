-- 为了避免 检查验证码 时出现的并发问题，封装的lua脚本
-- 获取传过来的key，也就是 phone_code:业务:手机号
local key = KEYS[1]
-- 该验证码可使用次数的key，指这个验证码还可以验证失败几次，防止攻击者暴力破解
-- ..是字符串拼接
local cntKey = key .. ":cnt"
-- 用户输入的验证码
local expectedCode = ARGV[1]

-- 还可以验证失败的次数
local cnt = tonumber(redis.call("get", cntKey))
-- 真正的验证码
local code = redis.call("get", key)

-- 验证错误次数耗尽 或者 这个字段莫名其妙没了
if cnt == nil or cnt <= 0 then
    return -1
end

if code == expectedCode then
    redis.call("set", cntKey, 0)
    return 0
else
    -- 用户输入验证码错误，验证失败的次数-1
    redis.call("decr", cntKey)
    return -2
end