-- 为了避免 检查数据 -> 发送验证码 时出现的并发问题，封装的lua脚本
-- 获取传过来的key，也就是 phone_code:业务:手机号
local key = KEYS[1]
-- 该验证码可使用次数的key，指这个验证码还可以验证失败几次，防止攻击者暴力破解
-- ..是字符串拼接
local cntKey = key .. ":cnt"
-- Go程序生成的验证码，也就是你准备存储的验证码
local val = ARGV[1]

-- 判断 Redis 中有没有这个key
-- 无需调用 get 方法，有个更简单的方法，因为归根结底你是要检测有效时间
-- 获取有效时间，并转成数字
local ttl = tonumber(redis.call("ttl", key))
-- -1 表示 key存在，但是没有过期时间
if ttl == -1 then
    -- 同事误操作了
    return -2
    -- -2：表示 key不存在
    -- ttl < 540：表示已经距离该用户上次发送验证码已经过去1分钟了
elseif ttl == -2 or ttl < 540 then
    -- 可以发送验证码
    redis.call("set", key, val)
    redis.call("expire", key, 600)
    redis.call("set", cntKey, 3)
    redis.call("expire", cntKey, 600)
    return 0
else
    -- 发送太频繁，1分钟内已经发送过一次验证码了
    return -1
end
