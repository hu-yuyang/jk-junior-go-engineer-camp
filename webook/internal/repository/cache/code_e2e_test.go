package cache

import (
	"context"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

// 单独测试 lua 脚本
func TestRedisUserCache_Set_e2e(t *testing.T) {
	rdb := redis.NewClient(&redis.Options{
		Addr: "localhost:16379",
	})
	testCases := []struct {
		name             string
		before           func(t *testing.T)
		after            func(t *testing.T)
		ctx              context.Context
		biz, phone, code string
		wantErr          error
	}{
		{
			name:   "设置成功",
			before: func(t *testing.T) {},
			after: func(t *testing.T) {
				ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
				defer cancelFunc()
				key := "phone_code:login:1234567890"

				duration, err := rdb.TTL(ctx, key).Result()
				assert.NoError(t, err)
				assert.True(t, duration > time.Minute*9+time.Second*50)

				code, err := rdb.GetDel(ctx, key).Result()
				// 这个验证码因为是随机的，我们并不知道，所以我们可以通过验证它的长度来确定有无
				assert.NoError(t, err)
				assert.Equal(t, "123456", code)
			},
			ctx:     context.Background(),
			biz:     "login",
			phone:   "1234567890",
			code:    "123456",
			wantErr: nil,
		},
		{
			name: "发送过于频繁",
			before: func(t *testing.T) {
				ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
				defer cancelFunc()
				key := "phone_code:login:1234567890"
				err := rdb.Set(ctx, key, "123456", time.Minute*9+time.Second*30).Err()
				assert.NoError(t, err)
			},
			after: func(t *testing.T) {
				ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
				defer cancelFunc()
				key := "phone_code:login:1234567890"
				code, err := rdb.GetDel(ctx, key).Result()
				// 这个验证码因为是随机的，我们并不知道，所以我们可以通过验证它的长度来确定有无
				assert.NoError(t, err)
				assert.Equal(t, "123456", code)
			},
			ctx:     context.Background(),
			biz:     "login",
			phone:   "1234567890",
			code:    "123456",
			wantErr: ErrCodeSendTooMany,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.before(t)
			defer tc.after(t)
			codeCache := NewCodeRedisCache(rdb)
			err := codeCache.Set(tc.ctx, tc.biz, tc.phone, tc.code)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}
