package cache

import (
	"context"
	"errors"
	"fmt"
	lru "github.com/hashicorp/golang-lru"
	"sync"
	"time"
)

// CodeLocalCache 本地缓存实现
type CodeLocalCache struct {
	cache *lru.Cache
	lock  sync.Mutex
	// rwLock     sync.RWMutex
	expiration time.Duration
}

type codeItem struct {
	code string
	// 当前验证码还可以重试的次数
	cnt int
	// 过期时间，以为 lru.cache 没有 redis 里 ttl 直接获取剩余过期时间的方式，
	// 所以这里需要维护一个，用来计算剩余时间
	expire time.Time
}

func NewCodeLocalCache(c *lru.Cache, expiration time.Duration) CodeCache {
	return &CodeLocalCache{
		cache:      c,
		expiration: expiration,
	}
}

// Set 缓存验证码
func (c *CodeLocalCache) Set(ctx context.Context, biz, phone, code string) error {
	// 这里你可以考虑读写锁优化，但是效果不会很好
	// 因为你可以预期，这里大部分都是走的写锁。
	c.lock.Lock()
	defer c.lock.Unlock()

	key := c.key(biz, phone)
	now := time.Now()
	value, ok := c.cache.Get(key)
	if !ok {
		c.cache.Add(key, codeItem{
			code:   code,
			cnt:    3,
			expire: now.Add(c.expiration),
		})
		return nil
	}
	item, ok := value.(codeItem)
	if !ok { // 理论上是进不来的
		return errors.New("系统错误")
	}
	if item.expire.Sub(now) > time.Minute*9 {
		// 距离上次发送小于1min
		return ErrCodeSendTooMany
	}
	// 再次发送验证码
	c.cache.Add(key, codeItem{
		code:   code,
		cnt:    3,
		expire: now.Add(c.expiration),
	})
	return nil
}

// Verify 验证验证码
func (c *CodeLocalCache) Verify(ctx context.Context, biz, phone, inputCode string) (bool, error) {
	c.lock.Lock()
	defer c.lock.Unlock()
	key := c.key(biz, phone)
	value, ok := c.cache.Get(key)
	if !ok { // 都没发送验证码
		return false, ErrKeyNotExist
	}
	item, ok := value.(codeItem)
	if !ok {
		return false, errors.New("系统错误")
	}
	if item.cnt <= 0 { // 验证失败次数过多，当前验证码已失效
		return false, ErrCodeVerifyTooMany
	}
	item.cnt--
	return item.code == inputCode, nil
}

// 拼接成的 phone_code:$biz:$phone 的形式
func (c *CodeLocalCache) key(biz, phone string) string {
	return fmt.Sprintf("phone_code:%s:%s", biz, phone)
}
