package cache

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/repository/cache/redismocks"
	"context"
	"errors"
	"fmt"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"testing"
)

func TestCodeRedisCache_Set(t *testing.T) {
	// 为什么把它抄过来，而不是直接去调这个私有的方法呢？
	// 是因为你调不了
	keyFunc := func(biz, phone string) string {
		return fmt.Sprintf("phone_code:%s:%s", biz, phone)
	}
	testCases := []struct {
		name             string
		mock             func(ctrl *gomock.Controller) redis.Cmdable
		ctx              context.Context
		biz, phone, code string
		wantErr          error
	}{
		{
			name: "设置成功",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := redismocks.NewMockCmdable(ctrl)
				// 注意 Eval 方法 实际上返回的是一个 Cmdable
				retCmd := redis.NewCmd(context.Background())
				// 模拟 .Int() 方法
				retCmd.SetErr(nil)
				retCmd.SetVal(int64(0))

				cmd.EXPECT().Eval(gomock.Any(), luaSetCode,
					[]string{keyFunc("test", "1234567889")},
					"123456",
				).Return(retCmd)
				return cmd
			},
			ctx:     context.Background(),
			biz:     "test",
			phone:   "1234567889",
			code:    "123456",
			wantErr: nil,
		},
		{
			name: "redis 出问题",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := redismocks.NewMockCmdable(ctrl)
				// 注意 Eval 方法 实际上返回的是一个 Cmdable
				retCmd := redis.NewCmd(context.Background())
				// 模拟 .Int() 方法
				retCmd.SetErr(errors.New("redis 出问题"))
				retCmd.SetVal(int64(0))

				cmd.EXPECT().Eval(gomock.Any(), luaSetCode,
					[]string{keyFunc("test", "1234567889")},
					"123456",
				).Return(retCmd)
				return cmd
			},
			ctx:     context.Background(),
			biz:     "test",
			phone:   "1234567889",
			code:    "123456",
			wantErr: errors.New("redis 出问题"),
		},
		{
			name: "验证码存在，但是没有过期时间",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := redismocks.NewMockCmdable(ctrl)
				// 注意 Eval 方法 实际上返回的是一个 Cmdable
				retCmd := redis.NewCmd(context.Background())
				// 模拟 .Int() 方法
				retCmd.SetVal(int64(-2))

				cmd.EXPECT().Eval(gomock.Any(), luaSetCode,
					[]string{keyFunc("test", "1234567889")},
					"123456",
				).Return(retCmd)
				return cmd
			},
			ctx:     context.Background(),
			biz:     "test",
			phone:   "1234567889",
			code:    "123456",
			wantErr: errors.New("验证码存在，但是没有过期时间"),
		},
		{
			name: "发送过于频繁",
			mock: func(ctrl *gomock.Controller) redis.Cmdable {
				cmd := redismocks.NewMockCmdable(ctrl)
				// 注意 Eval 方法 实际上返回的是一个 Cmdable
				retCmd := redis.NewCmd(context.Background())
				// 模拟 .Int() 方法
				retCmd.SetVal(int64(-1))

				cmd.EXPECT().Eval(gomock.Any(), luaSetCode,
					[]string{keyFunc("test", "1234567889")},
					"123456",
				).Return(retCmd)
				return cmd
			},
			ctx:     context.Background(),
			biz:     "test",
			phone:   "1234567889",
			code:    "123456",
			wantErr: ErrCodeSendTooMany,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			cmd := tc.mock(ctrl)
			codeCache := NewCodeRedisCache(cmd)
			err := codeCache.Set(tc.ctx, tc.biz, tc.phone, tc.code)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}
