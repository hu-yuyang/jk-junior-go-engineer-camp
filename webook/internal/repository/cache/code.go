package cache

import (
	"context"
	_ "embed"
	"errors"
	"fmt"
	"github.com/redis/go-redis/v9"
)

var (
	//go:embed lua/set_code.lua
	// 可以在编译阶段将静态资源文件打包进编译好的程序中，并提供访问这些文件的能力
	luaSetCode string
	//go:embed lua/verify_code.lua
	luaVerifyCode string

	ErrCodeSendTooMany   = errors.New("发送太频繁")
	ErrCodeVerifyTooMany = errors.New("验证太频繁")
)

// CodeCache 验证码业务专属缓存
type CodeCache interface {
	Set(ctx context.Context, biz, phone, code string) error
	Verify(ctx context.Context, biz, phone, code string) (bool, error)
}

// CodeRedisCache 验证码缓存-Redis 实现
type CodeRedisCache struct {
	cmd redis.Cmdable
}

func NewCodeRedisCache(cmd redis.Cmdable) CodeCache {
	return &CodeRedisCache{
		cmd: cmd,
	}
}

// Set 利用 Lua 脚本 写入验证码
// biz: 业务; phone: 目标手机号; code: 验证码;
func (c *CodeRedisCache) Set(ctx context.Context, biz, phone, code string) error {
	res, err := c.cmd.Eval(ctx, luaSetCode, []string{c.key(biz, phone)}, code).Int()
	if err != nil {
		// 调用 Redis 出了问题
		return err
	}
	switch res {
	case -2:
		return errors.New("验证码存在，但是没有过期时间")
	case -1:
		// 这是一个上游会关心的错误，所以要定义出来
		return ErrCodeSendTooMany
	default:
		// 发送成功
		return nil
	}
}

// Verify 验证验证码
func (c *CodeRedisCache) Verify(ctx context.Context, biz, phone, code string) (bool, error) {
	res, err := c.cmd.Eval(ctx, luaVerifyCode, []string{c.key(biz, phone)}, code).Int()
	if err != nil {
		// 调用 Redis 出了问题
		return false, err
	}
	switch res {
	case -2: // 用户输入验证码错误
		return false, nil
	case -1: // 验证过于频繁
		// 这是一个上游会关心的错误，所以要定义出来
		return false, ErrCodeVerifyTooMany
	default: // 验证成功
		return true, nil
	}
}

// 拼接成的 phone_code:$biz:$phone 的形式
func (c *CodeRedisCache) key(biz, phone string) string {
	return fmt.Sprintf("phone_code:%s:%s", biz, phone)
}
