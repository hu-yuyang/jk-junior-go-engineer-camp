// Package repository 帖子-制作库-演示用
package repository

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/domain"
	"context"
)

// ArticleAuthorRepository 帖子制作库
type ArticleAuthorRepository interface {
	Create(ctx context.Context, art domain.Article) (int64, error)
	Update(ctx context.Context, art domain.Article) error
}
