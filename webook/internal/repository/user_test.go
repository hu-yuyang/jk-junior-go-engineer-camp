package repository

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/domain"
	"JK-Junior-Go-Engineer-Camp/webook/internal/repository/cache"
	cachemocks "JK-Junior-Go-Engineer-Camp/webook/internal/repository/cache/mocks"
	"JK-Junior-Go-Engineer-Camp/webook/internal/repository/dao"
	daomocks "JK-Junior-Go-Engineer-Camp/webook/internal/repository/dao/mocks"
	"context"
	"database/sql"
	"errors"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"testing"
	"time"
)

func TestT(t *testing.T) {
	t.Log(time.Now().UnixMilli())
}
func TestCachedUserRepository_FindById(t *testing.T) {
	testCases := []struct {
		// 测试用例
		name string
		// mock NewCacheUserRepository 所需要的依赖
		mock func(ctrl *gomock.Controller) (cache.UserCache, dao.UserDAO)
		// 预期输入
		ctx context.Context
		uid int64
		// 预期输出
		wantUser domain.User
		wantErr  error
	}{
		{
			name: "查找成功，缓存未命中",
			mock: func(ctrl *gomock.Controller) (cache.UserCache, dao.UserDAO) {
				userCache := cachemocks.NewMockUserCache(ctrl)
				userDAO := daomocks.NewMockUserDAO(ctrl)
				mockUid := int64(123)
				userCache.EXPECT().Get(gomock.Any(), mockUid).
					Return(domain.User{}, cache.ErrKeyNotExist)
				userDAO.EXPECT().FindById(gomock.Any(), mockUid).
					// 这里写详细点为了测试 daoToDomain 方法
					Return(dao.User{
						Id:       mockUid,
						Email:    sql.NullString{String: "123@qq.com", Valid: true},
						Phone:    sql.NullString{String: "12345678", Valid: true},
						Password: "123",
						Birthday: 1709465359595,
						AboutMe:  "自我介绍",
						Ctime:    1709465359595,
						Utime:    1709465359595,
					}, nil)
				userCache.EXPECT().Set(gomock.Any(), domain.User{
					Id:       123,
					Email:    "123@qq.com",
					Phone:    "12345678",
					Password: "123",
					Birthday: time.UnixMilli(1709465359595),
					AboutMe:  "自我介绍",
					Ctime:    time.UnixMilli(1709465359595),
				}).Return(nil)
				return userCache, userDAO
			},
			ctx: context.Background(),
			uid: 123,
			wantUser: domain.User{
				Id:       123,
				Email:    "123@qq.com",
				Phone:    "12345678",
				Password: "123",
				Birthday: time.UnixMilli(1709465359595),
				AboutMe:  "自我介绍",
				Ctime:    time.UnixMilli(1709465359595),
			},
		},
		{
			name: "查找成功，缓存命中",
			mock: func(ctrl *gomock.Controller) (cache.UserCache, dao.UserDAO) {
				userCache := cachemocks.NewMockUserCache(ctrl)
				userDAO := daomocks.NewMockUserDAO(ctrl)
				mockUid := int64(123)
				userCache.EXPECT().Get(gomock.Any(), mockUid).
					Return(domain.User{Id: mockUid}, nil)
				return userCache, userDAO
			},
			ctx:      context.Background(),
			uid:      123,
			wantUser: domain.User{Id: 123},
		},
		{
			name: "未找到用户",
			mock: func(ctrl *gomock.Controller) (cache.UserCache, dao.UserDAO) {
				userCache := cachemocks.NewMockUserCache(ctrl)
				userDAO := daomocks.NewMockUserDAO(ctrl)
				mockUid := int64(123)
				userCache.EXPECT().Get(gomock.Any(), mockUid).
					Return(domain.User{}, cache.ErrKeyNotExist)
				userDAO.EXPECT().FindById(gomock.Any(), mockUid).
					Return(dao.User{}, dao.ErrRecordNotFound)
				return userCache, userDAO
			},
			ctx:     context.Background(),
			uid:     123,
			wantErr: dao.ErrRecordNotFound,
		},
		{
			name: "回写缓存失败",
			mock: func(ctrl *gomock.Controller) (cache.UserCache, dao.UserDAO) {
				userCache := cachemocks.NewMockUserCache(ctrl)
				userDAO := daomocks.NewMockUserDAO(ctrl)
				mockUid := int64(123)
				userCache.EXPECT().Get(gomock.Any(), mockUid).
					Return(domain.User{}, cache.ErrKeyNotExist)
				userDAO.EXPECT().FindById(gomock.Any(), mockUid).
					// 这里写详细点为了测试 daoToDomain 方法
					Return(dao.User{
						Id:       mockUid,
						Email:    sql.NullString{String: "123@qq.com", Valid: true},
						Phone:    sql.NullString{String: "12345678", Valid: true},
						Password: "123",
						Birthday: 1709465359595,
						AboutMe:  "自我介绍",
						Ctime:    1709465359595,
						Utime:    1709465359595,
					}, nil)
				userCache.EXPECT().Set(gomock.Any(), domain.User{
					Id:       123,
					Email:    "123@qq.com",
					Phone:    "12345678",
					Password: "123",
					Birthday: time.UnixMilli(1709465359595),
					AboutMe:  "自我介绍",
					Ctime:    time.UnixMilli(1709465359595),
				}).Return(errors.New("redis 错误"))
				return userCache, userDAO
			},
			ctx: context.Background(),
			uid: 123,
			wantUser: domain.User{
				Id:       123,
				Email:    "123@qq.com",
				Phone:    "12345678",
				Password: "123",
				Birthday: time.UnixMilli(1709465359595),
				AboutMe:  "自我介绍",
				Ctime:    time.UnixMilli(1709465359595),
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			userCache, userDAO := tc.mock(ctrl)
			userRepo := NewCachedUserRepository(userDAO, userCache)
			user, err := userRepo.FindById(tc.ctx, tc.uid)
			assert.Equal(t, tc.wantUser, user)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}
