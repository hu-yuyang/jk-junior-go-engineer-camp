// Package repository 帖子-线上库-演示用
package repository

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/domain"
	"context"
)

// ArticleReaderRepository 帖子线上库
type ArticleReaderRepository interface {
	Save(ctx context.Context, art domain.Article) error
}
