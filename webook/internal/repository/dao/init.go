package dao

import "gorm.io/gorm"

// InitTables 初始化表结构
func InitTables(db *gorm.DB) error {
	// 严格来说，这不是优秀的实践
	// 理论来说应该走公司审批的流程，而且让dao和gorm强耦合了，不能做到随机切换
	return db.AutoMigrate(&User{}, &Article{})
}
