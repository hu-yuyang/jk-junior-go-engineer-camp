package dao

import (
	"context"
	"gorm.io/gorm"
)

// ArticleReaderDAO 线上库
type ArticleReaderDAO interface {
	// Upsert 插入或者更新，这里偏向于数据库层面，所以不叫Save
	Upsert(context.Context, Article) error
	UpsertV2(context.Context, PublishedArticle) error
}

type ArticleReaderGORMDAO struct {
	db *gorm.DB
}

func NewArticleReaderGORMDAO(db *gorm.DB) ArticleReaderDAO {
	return &ArticleReaderGORMDAO{db: db}
}

func (a *ArticleReaderGORMDAO) Upsert(ctx context.Context, article Article) error {
	//TODO implement me
	panic("implement me")
}

func (a *ArticleReaderGORMDAO) UpsertV2(ctx context.Context, article PublishedArticle) error {
	//TODO implement me
	panic("implement me")
}
