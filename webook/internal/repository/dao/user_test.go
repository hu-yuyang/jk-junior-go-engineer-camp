package dao

import (
	"context"
	"database/sql"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	mysqldriver "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"testing"
)

func TestGORMUserDAO_Insert(t *testing.T) {
	testCases := []struct {
		name    string
		mock    func(t *testing.T) *sql.DB
		ctx     context.Context
		user    User
		wantErr error
	}{
		{
			name: "插入成功",
			mock: func(t *testing.T) *sql.DB {
				db, mock, err := sqlmock.New()
				assert.NoError(t, err)
				mockResult := sqlmock.NewResult(123, 1)
				mock.ExpectExec("INSERT INTO .*").
					WillReturnResult(mockResult)
				return db
			},
			ctx:  context.Background(),
			user: User{Nickname: "TOM"},
		},
		{
			name: "邮箱冲突",
			mock: func(t *testing.T) *sql.DB {
				db, mock, err := sqlmock.New()
				assert.NoError(t, err)
				mock.ExpectExec("INSERT INTO .*").
					WillReturnError(&mysqldriver.MySQLError{Number: 1062})
				return db
			},
			ctx:     context.Background(),
			user:    User{Nickname: "TOM"},
			wantErr: ErrDuplicateEmail,
		},
		{
			name: "插入失败，数据库错误",
			mock: func(t *testing.T) *sql.DB {
				db, mock, err := sqlmock.New()
				assert.NoError(t, err)
				mock.ExpectExec("INSERT INTO .*").
					WillReturnError(errors.New("数据库错误"))
				return db
			},
			ctx:     context.Background(),
			user:    User{Nickname: "TOM"},
			wantErr: errors.New("数据库错误"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			sqlDB := tc.mock(t)
			// 我们如何利用这个db，创建 gorm 的 db 呢？
			db, err := gorm.Open(mysql.New(mysql.Config{
				Conn: sqlDB,
				// 跳过查询数据库使用的版本
				SkipInitializeWithVersion: true,
			}), &gorm.Config{
				// 禁止 gorm 自动去 ping 数据库
				DisableAutomaticPing: true,
				/*
					首先，在数据库中 中一个 叫做 autocommit 的机制
					在 MySQL 中我们 Insert 只需要写一个 Insert 语句即可
					但是在有的其他的数据库中，例如 Oracle 中，你后续必须写一个 commit
					不让你插入后，只是你当前会话插入了，其他人看不到
					而所谓的 autocommit 就是 MySQL 会自动的帮你 commit
					而 gorm 为了兼容 支持这种和不支持这种的数据库，
					gorm会自动的帮你在语句前后加上 Begin; 和 Commit;
					而 SkipDefaultTransaction 就是让 gorm 不要加了
				*/
				SkipDefaultTransaction: true,
			})
			assert.NoError(t, err)
			userDAO := NewUserDAO(db)
			err = userDAO.Insert(tc.ctx, tc.user)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}
