package dao

import (
	"context"
	"gorm.io/gorm"
)

// ArticleAuthorDAO 制作库
type ArticleAuthorDAO interface {
	Create(context.Context, Article) (int64, error)
	Update(context.Context, Article) error
}

// ArticleAuthorGORMDAO GORM实现
type ArticleAuthorGORMDAO struct {
	db *gorm.DB
}

func NewArticleAuthorGORMDAO(db *gorm.DB) ArticleAuthorDAO {
	return &ArticleAuthorGORMDAO{db: db}
}

func (a ArticleAuthorGORMDAO) Create(ctx context.Context, article Article) (int64, error) {
	//TODO implement me
	panic("implement me")
}

func (a ArticleAuthorGORMDAO) Update(ctx context.Context, article Article) error {
	//TODO implement me
	panic("implement me")
}
