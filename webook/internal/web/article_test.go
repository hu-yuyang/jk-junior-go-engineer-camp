package web

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/domain"
	"JK-Junior-Go-Engineer-Camp/webook/internal/service"
	svcmocks "JK-Junior-Go-Engineer-Camp/webook/internal/service/mocks"
	ijwt "JK-Junior-Go-Engineer-Camp/webook/internal/web/jwt"
	"JK-Junior-Go-Engineer-Camp/webook/pkg/logger"
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestArticleHandler_Publish(t *testing.T) {
	testCases := []struct {
		name string
		// 准备mock数据
		mock    func(ctrl *gomock.Controller) service.ArticleService
		reqBody string // 预期请求

		wantCode int    // 预期响应码
		wantRes  Result // 预期响应
	}{
		{
			name: "新建并发表成功",
			mock: func(ctrl *gomock.Controller) service.ArticleService {
				svc := svcmocks.NewMockArticleService(ctrl)
				// 设计预期ArticleService的Publish的逻辑
				svc.EXPECT().Publish(gomock.Any(), domain.Article{
					Title:   "标题",
					Content: "内容",
					Author:  domain.Author{Id: 123},
				}).Return(int64(1), nil)
				return svc
			},
			reqBody:  `{"title":"标题", "content":"内容"}`,
			wantCode: http.StatusOK,
			// 注意这里，json中数字在转any的时候，默认用的是float64
			wantRes: Result{Data: float64(1)},
		},
		{
			name: "修改已有帖子，而后发表成功",
			mock: func(ctrl *gomock.Controller) service.ArticleService {
				svc := svcmocks.NewMockArticleService(ctrl)
				svc.EXPECT().Publish(gomock.Any(), domain.Article{
					Id:      1,
					Title:   "标题",
					Content: "内容",
					Author:  domain.Author{Id: 123},
				}).Return(int64(1), nil)
				return svc
			},
			reqBody:  `{"id":1, "title":"标题", "content":"内容"}`,
			wantCode: http.StatusOK,
			// 注意这里，json中数字在转any的时候，默认用的是float64
			wantRes: Result{Data: float64(1)},
		},
		{
			name: "发表失败",
			mock: func(ctrl *gomock.Controller) service.ArticleService {
				svc := svcmocks.NewMockArticleService(ctrl)
				svc.EXPECT().Publish(gomock.Any(), domain.Article{
					Title:   "标题",
					Content: "内容",
					Author:  domain.Author{Id: 123},
				}).Return(int64(0), errors.New("模拟发表失败"))
				return svc
			},
			reqBody:  `{"title":"标题", "content":"内容"}`,
			wantCode: http.StatusOK,
			// 注意这里，json中数字在转any的时候，默认用的是float64
			wantRes: Result{
				Code: 5, // 后面会讲标准的错误码设计
				Msg:  "系统错误",
			},
		},
		{
			name: "Bind错误",
			mock: func(ctrl *gomock.Controller) service.ArticleService {
				svc := svcmocks.NewMockArticleService(ctrl)
				return svc
			},
			reqBody:  `{"title":"标题", "content":"内容"1111}`,
			wantCode: http.StatusBadRequest,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			// 1. 构造Handler
			artSvc := tc.mock(ctrl)
			artHdl := NewArticleHandler(artSvc, logger.NewNopLogger())

			// 2. 准备服务器，注册路由
			server := gin.Default()
			// 直接设置登录态
			server.Use(func(ctx *gin.Context) {
				ctx.Set("user", ijwt.UserClaims{UserId: 123})
			})
			artHdl.RegisterRoutes(server)

			// 3. 准备Req和记录的recorder
			req, err := http.NewRequest(http.MethodPost,
				"/articles/publish", bytes.NewBufferString(tc.reqBody))
			assert.NoError(t, err)
			req.Header.Set("Content-Type", "application/json")
			recorder := httptest.NewRecorder()

			// 4. 发起请求，执行
			server.ServeHTTP(recorder, req)

			// 5. 断言
			assert.Equal(t, tc.wantCode, recorder.Code)
			if tc.wantCode != http.StatusOK {
				return
			}
			var res Result
			err = json.NewDecoder(recorder.Body).Decode(&res)
			assert.NoError(t, err)
			assert.Equal(t, tc.wantRes, res)
		})
	}
}
