package middleware

import (
	"encoding/gob"
	"fmt"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

type LoginMiddlewareBuilder struct {
}

// CheckLogin 使用了Session进行登录校验
func (m *LoginMiddlewareBuilder) CheckLogin() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		// 忽略掉 登录和注册接口
		path := ctx.Request.URL.Path
		if path == "/users/signup" || path == "/users/login" {
			// 不需要登录校验
			return
		}

		session := sessions.Default(ctx)
		userId := session.Get("userId")
		if userId == nil {
			// 中断，不要往后执行了，401: 没权限访问资源
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		// 刷新登录状态
		gob.Register(time.Now()) // 需要注册一下这个类型
		now := time.Now()
		const updateTimeKey = "update_time"
		// 尝试拿出上次刷新时间
		get := session.Get(updateTimeKey)
		lastUpdateTime, ok := get.(time.Time)
		// 意味着你这是第一次进来 || 断言失败（和情况1有些重合） || 距离上次刷新过了一分钟
		if get == nil || !ok || now.Sub(lastUpdateTime) > time.Minute {
			session.Set(updateTimeKey, now)
			// Gin的session的set是覆盖性质的，所以这里还需要set一下userId
			session.Set("userId", userId)
			// 不要忘记Save
			err := session.Save()
			if err != nil {
				// 打日志，或者 不要这个err，用_接收
				// 为什么不是中断执行呢？因为并不影响业务的正常使用
				fmt.Println(err)
			}
		}
	}
}
