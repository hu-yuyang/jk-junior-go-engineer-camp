package web

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/domain"
	"JK-Junior-Go-Engineer-Camp/webook/internal/service"
	svcmocks "JK-Junior-Go-Engineer-Camp/webook/internal/service/mocks"
	"bytes"
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMock(t *testing.T) {
	// 1. 创建一个控制 mock 的控制器
	ctrl := gomock.NewController(t)
	// 每一个测试结束都需要调用 Finish() 方法
	// 然后 mock 就会验证 你的测试流程是否符合预期
	defer ctrl.Finish()
	// 2. 创建模拟调用的对象
	userSvc := svcmocks.NewMockUserService(ctrl)
	// 3. 设计预期中的模拟场景
	// 这里是预期第一个参数是任何参数都没关系，我不关心，但是我预期第二个参数一定是下面这个
	userSvc.EXPECT().Signup(gomock.Any(),
		domain.User{Email: "123@qq.com"},
		// 返回值
		//).Return(nil)
	).Return(errors.New("模拟错误"))
	userSvc.EXPECT().Profile(gomock.Any(), int64(123)).
		Return(domain.User{Id: 123, Email: "123"}, nil)

	// 4. 模拟
	// 错误分支：参数2对不上预期，会报错
	//err := userSvc.Signup(context.Background(), domain.User{Email: "123"})
	// 正确分支  err = 模拟错误
	err := userSvc.Signup(context.Background(), domain.User{Email: "123@qq.com"})
	t.Log(err)
	user, err := userSvc.Profile(context.Background(), 123)
	t.Log(err, user)
}

func TestUserHandler_SignUp(t *testing.T) {
	testCases := []struct {
		// 用例名称，简单说明测试场景
		name string

		// 准备 mock 数据
		// 参数 ctrl 是因为你需要用这个东西来初始化你的 mock 对象
		// 因为 UserHandler 用到了 UserService 和 CodeService
		// 所以我们需要准备这两个的 mock 实例，并返回
		mock func(ctrl *gomock.Controller) (service.UserService, service.CodeService)

		// 预期输入/请求
		// 因为 Request 的构建可能很复杂，所以我们这里直接定义一个 Builder
		// 这个  t 是 后续 Run(t.name,func(t *testing.T)) 里的 t
		reqBuilder func(t *testing.T) *http.Request

		// 预期输出/响应
		wantCode int
		wantBody string
	}{
		{
			name: "注册成功",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				userService := svcmocks.NewMockUserService(ctrl)
				userService.EXPECT().Signup(gomock.Any(), domain.User{
					Email:    "123@qq.com",
					Password: "Hyy123@@",
				}).Return(nil)
				return userService, nil
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost,
					"/users/signup",
					bytes.NewReader([]byte(`{
						"email": "123@qq.com",
						"password": "Hyy123@@",
						"confirmPassword": "Hyy123@@"
					}`)))
				// 注意，我们使用的 Bind 方法，是根据你的 Content-type 进行数据绑定的
				// 这是很多人容易犯错的地方
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "注册成功",
		},
		{
			name: "Bind 方法出错",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				userService := svcmocks.NewMockUserService(ctrl)
				// 因为 Bind 方法出错，不会走到 SignUp 那边，所以不用设置对应的预期模拟场景
				return userService, nil
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost,
					"/users/signup",
					bytes.NewReader([]byte(`错误JSON串`)))
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusBadRequest,
		},
		{
			name: "邮箱格式错误",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				userService := svcmocks.NewMockUserService(ctrl)
				return userService, nil
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost,
					"/users/signup",
					bytes.NewReader([]byte(`{
						"email": "123.com",
						"password": "Hyy123@@",
						"confirmPassword": "Hyy123@@"
					}`)))
				// 注意，我们使用的 Bind 方法，是根据你的 Content-type 进行数据绑定的
				// 这是很多人容易犯错的地方
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "邮箱格式错误",
		},
		{
			name: "两次密码不匹配",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				userService := svcmocks.NewMockUserService(ctrl)
				return userService, nil
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost,
					"/users/signup",
					bytes.NewReader([]byte(`{
						"email": "123@qq.com",
						"password": "Hyy123",
						"confirmPassword": "Hyy123@@"
					}`)))
				// 注意，我们使用的 Bind 方法，是根据你的 Content-type 进行数据绑定的
				// 这是很多人容易犯错的地方
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "两次密码不匹配",
		},
		{
			name: "密码格式错误",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				userService := svcmocks.NewMockUserService(ctrl)
				return userService, nil
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost,
					"/users/signup",
					bytes.NewReader([]byte(`{
						"email": "123@qq.com",
						"password": "Hyy123",
						"confirmPassword": "Hyy123"
					}`)))
				// 注意，我们使用的 Bind 方法，是根据你的 Content-type 进行数据绑定的
				// 这是很多人容易犯错的地方
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "密码必须包含字母、数字、特殊字符，并且不少于8位，不超过72位",
		},
		{
			name: "系统错误",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				userService := svcmocks.NewMockUserService(ctrl)
				userService.EXPECT().Signup(gomock.Any(), domain.User{
					Email:    "123@qq.com",
					Password: "Hyy123@@",
				}).Return(errors.New("DB错误"))
				return userService, nil
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost,
					"/users/signup",
					bytes.NewReader([]byte(`{
						"email": "123@qq.com",
						"password": "Hyy123@@",
						"confirmPassword": "Hyy123@@"
					}`)))
				// 注意，我们使用的 Bind 方法，是根据你的 Content-type 进行数据绑定的
				// 这是很多人容易犯错的地方
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "系统错误",
		},
		{
			name: "邮箱冲突",
			mock: func(ctrl *gomock.Controller) (service.UserService, service.CodeService) {
				userService := svcmocks.NewMockUserService(ctrl)
				userService.EXPECT().Signup(gomock.Any(), domain.User{
					Email:    "123@qq.com",
					Password: "Hyy123@@",
				}).Return(service.ErrDuplicateEmail)
				return userService, nil
			},
			reqBuilder: func(t *testing.T) *http.Request {
				req, err := http.NewRequest(http.MethodPost,
					"/users/signup",
					bytes.NewReader([]byte(`{
						"email": "123@qq.com",
						"password": "Hyy123@@",
						"confirmPassword": "Hyy123@@"
					}`)))
				// 注意，我们使用的 Bind 方法，是根据你的 Content-type 进行数据绑定的
				// 这是很多人容易犯错的地方
				req.Header.Set("Content-Type", "application/json")
				assert.NoError(t, err)
				return req
			},
			wantCode: http.StatusOK,
			wantBody: "该邮箱已被注册",
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// 1.初始化mock控制器
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			// 2. 利用 mock 来初始化 UserHandler
			userSvc, codeSvc := tc.mock(ctrl)
			hdl := NewUserHandler(userSvc, codeSvc, nil)
			// 3.注册路由
			gin.SetMode("release")
			server := gin.Default()

			hdl.RegisterRoutes(server)
			// 4. 准备请求
			req := tc.reqBuilder(t)
			// 5. 准备记录响应
			recorder := httptest.NewRecorder()
			// 6. 执行
			server.ServeHTTP(recorder, req)
			// 7. 断言结果
			assert.Equal(t, tc.wantCode, recorder.Code)
			assert.Equal(t, tc.wantBody, recorder.Body.String())
		})
	}
}
