package jwt

import (
	"github.com/gin-gonic/gin"
)

type Handler interface {
	// ExtractToken 从 authorization（Bearer xxx） 中提取 token
	ExtractToken(ctx *gin.Context) string
	// SetLoginToken 作为登录后设置 token 的统一逻辑
	SetLoginToken(ctx *gin.Context, uid int64) error
	// SetJWTToken 生成JWT token 并设置到 Header 中
	SetJWTToken(ctx *gin.Context, uid int64, ssid string) error
	// CheckSession  检查 ssid 是否有效
	CheckSession(ctx *gin.Context, ssid string) error
	// ClearToken 清除jwt并将ssid设置为无效
	ClearToken(ctx *gin.Context) error
}
