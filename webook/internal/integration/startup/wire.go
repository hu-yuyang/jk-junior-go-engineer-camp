//go:build wireinject

package startup

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/repository"
	"JK-Junior-Go-Engineer-Camp/webook/internal/repository/cache"
	"JK-Junior-Go-Engineer-Camp/webook/internal/repository/dao"
	"JK-Junior-Go-Engineer-Camp/webook/internal/service"
	"JK-Junior-Go-Engineer-Camp/webook/internal/web"
	ijwt "JK-Junior-Go-Engineer-Camp/webook/internal/web/jwt"
	"JK-Junior-Go-Engineer-Camp/webook/ioc"
	"github.com/gin-gonic/gin"
	"github.com/google/wire"
)

var thirdPartySet = wire.NewSet(
	InitDB,
	InitRedis,
	InitLogger,
)

// InitWebService 初始化web服务器-集成测试
func InitWebServer() *gin.Engine {
	wire.Build(
		// 第三方依赖
		thirdPartySet,
		// DAO 部分
		dao.NewUserDAO, dao.NewArticleGORMDAO,
		// Cache 部分
		cache.NewCodeRedisCache, cache.NewRedisUserCache,
		// repository 部分
		repository.NewCachedCodeRepository, repository.NewCachedUserRepository,
		repository.NewCachedArticleRepository,
		// service 部分
		ioc.InitSMSService, service.NewUserService, service.NewCodeService,
		service.NewArticleService, InitWechatService,
		// handler 部分
		web.NewUserHandler, web.NewOAuth2WechatHandler, web.NewArticleHandler,
		ijwt.NewRedisJwtHandler,
		// gin Engine 部分
		ioc.InitGinMiddlewares, ioc.InitWebServer,
	)
	return gin.Default()
}

func InitArticleHandler() *web.ArticleHandler {
	wire.Build(
		thirdPartySet,
		dao.NewArticleGORMDAO,
		repository.NewCachedArticleRepository,
		service.NewArticleService,
		web.NewArticleHandler,
	)
	return &web.ArticleHandler{}
}
