package startup

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/repository/dao"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// InitDB 初始化数据库-集成测试
func InitDB() *gorm.DB {
	db, err := gorm.Open(
		mysql.Open("root:root@tcp(localhost:13316)/webook"),
		&gorm.Config{},
	)
	if err != nil {
		panic(err)
	}
	err = dao.InitTables(db)
	if err != nil {
		panic(err)
	}
	return db
}
