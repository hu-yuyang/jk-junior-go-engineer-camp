package startup

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/service/oauth2/wechat"
	"JK-Junior-Go-Engineer-Camp/webook/pkg/logger"
)

func InitWechatService(l logger.LoggerV1) wechat.Service {
	return wechat.NewService("appId", "appSecret", l)
}
