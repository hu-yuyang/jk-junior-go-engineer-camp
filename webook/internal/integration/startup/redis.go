package startup

import (
	"github.com/redis/go-redis/v9"
)

// InitRedis 初始化 Redis-集成测试
func InitRedis() redis.Cmdable {
	redisClient := redis.NewClient(&redis.Options{
		Addr: "localhost:16379",
	})
	return redisClient
}
