package integration

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/integration/startup"
	"JK-Junior-Go-Engineer-Camp/webook/pkg/ginx"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func init() {
	gin.SetMode(gin.ReleaseMode)
}

func TestUserHandler_SendLoginSMSCode(t *testing.T) {
	rdb := startup.InitRedis()
	server := startup.InitWebServer()
	testCases := []struct {
		// 1.测试用例名称
		name string
		// 2.准备数据
		before func(t *testing.T)
		// 3.验证并删除数据
		// 注意这里不是验证你的返回值，你要验证的是 MySQL 或 Redis 里的数据对不对
		after func(t *testing.T)
		// 4.输入/请求
		// 这里由于前端只需要传入一个手机号，就直接定义了。
		// 标准的做法应该定义为 http.Request
		phone string
		// 5.预期输出/响应
		wantCode int
		wantBody ginx.Result
	}{
		{
			name: "发送成功",
			// 不需要准备数据
			before: func(t *testing.T) {},
			// 需要验证 Redis 中有这个数据，而且有正确的过期时间，最后给数据删除
			after: func(t *testing.T) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
				defer cancel()
				key := "phone_code:login:1234567890"
				code, err := rdb.Get(ctx, key).Result()
				// 这个验证码因为是随机的，我们并不知道，所以我们可以通过验证它的长度来确定有无
				assert.NoError(t, err)
				assert.True(t, len(code) > 0)
				duration, err := rdb.TTL(ctx, key).Result()
				assert.NoError(t, err)
				assert.True(t, duration > time.Minute*9+time.Second*50)
				err = rdb.Del(ctx, key).Err()
				assert.NoError(t, err)
			},
			phone:    "1234567890",
			wantCode: http.StatusOK,
			wantBody: ginx.Result{Msg: "发送成功"},
		},
		{
			name:     "未输入手机号",
			before:   func(t *testing.T) {},
			after:    func(t *testing.T) {},
			phone:    "",
			wantCode: http.StatusOK,
			wantBody: ginx.Result{
				Code: 4, // 代表用户端输入错误
				Msg:  "请输入手机号",
			},
		},
		{
			name: "发送过于频繁",
			before: func(t *testing.T) {
				ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
				defer cancelFunc()
				key := "phone_code:login:1234567890"
				err := rdb.Set(ctx, key, "123456", time.Minute*9+time.Second*30).Err()
				assert.NoError(t, err)
			},
			after: func(t *testing.T) {
				ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
				defer cancelFunc()
				key := "phone_code:login:1234567890"
				code, err := rdb.GetDel(ctx, key).Result()
				// 这个验证码因为是随机的，我们并不知道，所以我们可以通过验证它的长度来确定有无
				assert.NoError(t, err)
				assert.Equal(t, "123456", code)
			},
			phone:    "1234567890",
			wantCode: http.StatusOK,
			wantBody: ginx.Result{
				Code: 4,
				Msg:  "短信发送太频繁，请稍后再试",
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// 准备数据
			tc.before(t)
			// 验证并删除数据
			defer tc.after(t)

			// 准备请求
			req, err := http.NewRequest(http.MethodPost,
				"/users/login_sms/code/send",
				bytes.NewReader([]byte(
					fmt.Sprintf(`{"phone": "%s"}`, tc.phone),
				)),
			)
			req.Header.Set("Content-Type", "application/json")
			assert.NoError(t, err)

			// 准备响应
			recorder := httptest.NewRecorder()

			// 执行
			server.ServeHTTP(recorder, req)

			// 断言结果
			assert.Equal(t, tc.wantCode, recorder.Code)
			if recorder.Code != http.StatusOK {
				return
			}
			var resp ginx.Result
			// 反序列化为 Go 结构体
			// Unmarshal: 从字节切片解析，Decode: 适用于从一个 io.Reader中解析
			err = json.NewDecoder(recorder.Body).Decode(&resp)

			assert.NoError(t, err)
			assert.Equal(t, tc.wantBody, resp)
		})
	}
}
