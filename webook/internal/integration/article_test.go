// 集成测试-文章
package integration

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/integration/startup"
	"JK-Junior-Go-Engineer-Camp/webook/internal/repository/dao"
	ijwt "JK-Junior-Go-Engineer-Camp/webook/internal/web/jwt"
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"
	"net/http"
	"net/http/httptest"
	"testing"
)

// 测试套件
// 继承了 testify 库中的 Suite 接口
// 可以利用 testify 库提供的便捷测试功能，
// 如 SetupTest, TearDownTest 等方法来管理测试前的准备和清理工作。
type ArticleHandlerSuite struct {
	suite.Suite
	db     *gorm.DB
	server *gin.Engine
}

// 测试前的准备工作，将在套件中的每个测试之前运行
func (a *ArticleHandlerSuite) SetupTest() {
	a.db = startup.InitDB()
	artHdl := startup.InitArticleHandler()
	server := gin.Default()
	server.Use(func(ctx *gin.Context) { // 直接设置登录态
		ctx.Set("user", ijwt.UserClaims{UserId: 123})
	})
	artHdl.RegisterRoutes(server)
	a.server = server
}

// 清理工作，将在套件中的每个测试之后运行
func (a *ArticleHandlerSuite) TearDownTest() {
	a.db.Exec("truncate table `articles`")
}

func (a *ArticleHandlerSuite) TestEdit() {
	t := a.T()
	testCases := []struct {
		name     string             // 用例名称
		before   func(t *testing.T) // 测试用例前置条件，准备数据
		after    func(t *testing.T) // 测试用例后置条件，验证并删除数据
		art      Article            // 测试用例的参数
		wantCode int                // 期望状态码
		wantRes  Result[int64]      // 期望响应
	}{
		{
			name:   "新增帖子",
			before: func(t *testing.T) {},
			after: func(t *testing.T) {
				// 验证帖子保存到了数据库中，并删除
				var art dao.Article
				err := a.db.Where("author_id=?", 123).First(&art).Error
				assert.NoError(t, err)
				assert.True(t, art.Ctime > 0) // 断言创建时间是有的
				assert.True(t, art.Utime > 0) // 断言更新时间是有的
				assert.True(t, art.Id > 0)
				assert.Equal(t, "测试标题", art.Title)
				assert.Equal(t, int64(123), art.AuthorId)
				// 清理数据，数据库如果涉及自增主键，推荐使用truncate
				//a.TearDownTest()
			},
			art: Article{
				Title:   "测试标题",
				Content: "测试内容",
			},
			wantCode: http.StatusOK,
			wantRes: Result[int64]{
				Data: 1,
			},
		},
		{
			name: "修改帖子",
			before: func(t *testing.T) {
				err := a.db.Create(&dao.Article{
					Id:       2,
					Title:    "测试标题",
					Content:  "测试内容",
					AuthorId: 123,
					Ctime:    123,
					Utime:    123,
				}).Error
				assert.NoError(t, err)
			},
			after: func(t *testing.T) {
				var art dao.Article
				err := a.db.Where("id=?", 2).First(&art).Error
				assert.NoError(t, err)
				assert.True(t, art.Utime > 123)
				art.Utime = 0
				assert.Equal(t, dao.Article{
					Id:       2,
					Title:    "新的标题",
					Content:  "新的内容",
					Ctime:    123,
					AuthorId: 123,
				}, art)
			},
			art: Article{
				Id:      2,
				Title:   "新的标题",
				Content: "新的内容",
			},
			wantCode: http.StatusOK,
			wantRes: Result[int64]{
				Data: 2,
			},
		},
		{
			name: "修改帖子场景：修改别人帖子",
			before: func(t *testing.T) {
				err := a.db.Create(&dao.Article{
					Id:       3,
					Title:    "测试标题",
					Content:  "测试内容",
					AuthorId: 234,
					Ctime:    123,
					Utime:    123,
				}).Error
				assert.NoError(t, err)
			},
			after: func(t *testing.T) {
				// 应该验证数据没有变
				var art dao.Article
				err := a.db.Where("id=?", 3).First(&art).Error
				assert.NoError(t, err)
				assert.Equal(t, dao.Article{
					Id:       3,
					Title:    "测试标题",
					Content:  "测试内容",
					AuthorId: 234,
					Ctime:    123,
					Utime:    123,
				}, art)
			},
			art: Article{
				Id:      3,
				Title:   "新的标题",
				Content: "新的内容",
			},
			wantCode: http.StatusOK,
			wantRes: Result[int64]{
				Code: 5,
				Msg:  "系统错误",
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.before(t)
			defer tc.after(t)
			reqBody, err := json.Marshal(tc.art)
			assert.NoError(t, err)
			req, err := http.NewRequest(http.MethodPost,
				"/articles/edit",
				bytes.NewReader(reqBody))
			assert.NoError(t, err)
			req.Header.Set("Content-Type", "application/json")

			// 发送请求，记录响应
			recorder := httptest.NewRecorder()
			a.server.ServeHTTP(recorder, req)
			assert.Equal(t, tc.wantCode, recorder.Code)
			if tc.wantCode != http.StatusOK {
				return
			}
			var res Result[int64]
			err = json.NewDecoder(recorder.Body).Decode(&res)
			assert.NoError(t, err)
			assert.Equal(t, tc.wantRes, res)
		})
	}
}

// 测试套件入口
func TestArticleHandler(t *testing.T) {
	suite.Run(t, &ArticleHandlerSuite{})
}

// 使用泛型的 Result 结构体能够确保 Data 字段总是与预期的数据类型相匹配
type Result[T any] struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data T      `json:"data"`
}

type Article struct {
	Id      int64  `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
}
