package service

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/repository"
	"JK-Junior-Go-Engineer-Camp/webook/internal/service/sms"
	"context"
	"fmt"
	"math/rand"
)

var ErrCodeSendTooMany = repository.ErrCodeSendTooMany

type CodeService interface {
	Send(ctx context.Context, biz, phone string) error
	Verify(ctx context.Context, biz, phone, inputCode string) (bool, error)
}

type codeService struct {
	repo repository.CodeRepository
	sms  sms.Service
}

func NewCodeService(repo repository.CodeRepository, smsSvc sms.Service) CodeService {
	return &codeService{
		repo: repo,
		sms:  smsSvc,
	}
}

// Send 发送一个随机验证码
// biz：业务 phone：目标手机号
func (svc *codeService) Send(ctx context.Context, biz, phone string) error {
	code := svc.generateCode()
	err := svc.repo.Set(ctx, biz, phone, code)
	if err != nil {
		return err
	}
	// 开始发短信
	const templateId = "18877556"
	return svc.sms.Send(ctx, templateId, []string{code}, phone)
}

// Verify 验证验证码
// biz：业务 phone：目标手机号 inputCode：用户输入的验证码
func (svc *codeService) Verify(ctx context.Context, biz, phone, inputCode string) (bool, error) {
	ok, err := svc.repo.Verify(ctx, biz, phone, inputCode)
	if err == repository.ErrCodeVerifyTooMany {
		// 这样做，相当于我们对外面屏蔽了验证次数过多的错误
		// 我们就是告诉调用者，你输入的不对，避免暴力破解
		return false, nil
	}
	return ok, nil
}

// 初始化验证码
func (svc *codeService) generateCode() string {
	// 0-999999
	code := rand.Intn(1000000)
	return fmt.Sprintf("%06d", code)
}
