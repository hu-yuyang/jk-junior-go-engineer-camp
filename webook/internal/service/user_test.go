package service

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/domain"
	"JK-Junior-Go-Engineer-Camp/webook/internal/repository"
	repomocks "JK-Junior-Go-Engineer-Camp/webook/internal/repository/mocks"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"golang.org/x/crypto/bcrypt"
	"testing"
)

func TestPasswordEncrypt(t *testing.T) {
	pwd := []byte("Hyy123123@@")
	// Cost 代价，默认是10，代价越高，越难被破解，越消耗CPU
	// 加密算法是非常消耗CPU的
	encryptedPwd, err := bcrypt.GenerateFromPassword(pwd, bcrypt.DefaultCost)
	// 断言这里没有返回错误，即nil
	assert.NoError(t, err)
	t.Log(string(encryptedPwd))

	// 比较加密后的 和 明文，如果一样才返回nil
	//err = bcrypt.CompareHashAndPassword(encryptedPwd, []byte("wrong pwd"))
	// 断言err!=nil
	// 打个断点，会发现 err的值:
	// crypto/bcrypt: hashedPassword is not the hash of the given password
	//assert.NotNil(t, err)

	//err = bcrypt.CompareHashAndPassword(encryptedPwd, []byte("H123456@@"))
	//assert.NoError(t, err)
}

func TestUserService_Login(t *testing.T) {
	testCases := []struct {
		// 1. 用例名称
		name string
		// 2. 准备 mock 数据
		mock func(ctrl *gomock.Controller) repository.UserRepository
		// 3. 预期输入
		ctx             context.Context
		email, password string
		// 4. 预期输出
		wantUser domain.User
		wantErr  error
	}{
		{
			name: "登录成功",
			mock: func(ctrl *gomock.Controller) repository.UserRepository {
				userRepo := repomocks.NewMockUserRepository(ctrl)
				userRepo.EXPECT().FindByEmail(gomock.Any(), "123@qq.com").
					Return(domain.User{
						Email: "123@qq.com",
						// 这个密码是加密后的，通过上面的 TestPasswordEncrypt 生成的
						Password: "$2a$10$wNbfJ9f3QerIvwYILtAVQ.IDvQDYhkglw5RqNCNnY32Yj5tFKbSV2",
					}, nil)
				return userRepo
			},
			ctx:      context.Background(),
			email:    "123@qq.com",
			password: "Hyy123@@",
			wantUser: domain.User{
				Email:    "123@qq.com",
				Password: "$2a$10$wNbfJ9f3QerIvwYILtAVQ.IDvQDYhkglw5RqNCNnY32Yj5tFKbSV2",
			},
		},
		{
			name: "用户不存在",
			mock: func(ctrl *gomock.Controller) repository.UserRepository {
				userRepo := repomocks.NewMockUserRepository(ctrl)
				userRepo.EXPECT().FindByEmail(gomock.Any(), "123@qq.com").
					Return(domain.User{}, repository.ErrUserNotFound)
				return userRepo
			},
			ctx:      context.Background(),
			email:    "123@qq.com",
			password: "Hyy123@@",
			wantErr:  ErrInvalidUserOrPassword,
		},
		{
			name: "系统错误",
			mock: func(ctrl *gomock.Controller) repository.UserRepository {
				userRepo := repomocks.NewMockUserRepository(ctrl)
				userRepo.EXPECT().FindByEmail(gomock.Any(), "123@qq.com").
					Return(domain.User{}, errors.New("DB 错误"))
				return userRepo
			},
			ctx:      context.Background(),
			email:    "123@qq.com",
			password: "Hyy123@@",
			wantErr:  errors.New("DB 错误"),
		},
		{
			name: "密码错误",
			mock: func(ctrl *gomock.Controller) repository.UserRepository {
				userRepo := repomocks.NewMockUserRepository(ctrl)
				userRepo.EXPECT().FindByEmail(gomock.Any(), "123@qq.com").
					Return(domain.User{
						Email: "123@qq.com",
						// 这个密码是加密后的，通过上面的 TestPasswordEncrypt 生成的
						Password: "$2a$10$wNbfJ9f3QerIvwYILtAVQ.IDvQDYhkglw5RqNCNnY32Yj5tFKbSV2",
					}, nil)
				return userRepo
			},
			ctx:      context.Background(),
			email:    "123@qq.com",
			password: "Hyy123123@@",
			wantErr:  ErrInvalidUserOrPassword,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			userRepository := tc.mock(ctrl)
			userSvc := NewUserService(userRepository)
			user, err := userSvc.Login(tc.ctx, tc.email, tc.password)
			assert.Equal(t, tc.wantUser, user)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}
