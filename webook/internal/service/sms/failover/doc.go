// Package failover 短信服务的切换服务商（故障转移）装饰器
// 策略一：failover，如果出现错误了，就直接换一个服务商，进行重试
// 策略二：连续 N 个超时就切换
package failover
