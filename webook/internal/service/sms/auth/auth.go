package auth

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/service/sms"
	"context"
	"github.com/golang-jwt/jwt/v5"
)

type AuthSMSService struct {
	svc sms.Service
	key []byte // token 的密钥
}

func NewAuthSMSService(svc sms.Service) *AuthSMSService {
	return &AuthSMSService{
		svc: svc,
	}
}

// AuthSMSClaims token 中的数据
type AuthSMSClaims struct {
	jwt.RegisteredClaims
	Tpl string
	// 你可以额外加字段，比如一些辅助身份认证的信息等等
}

func (a AuthSMSService) Send(ctx context.Context, tplToken string,
	args []string, number ...string) error {
	var claims AuthSMSClaims
	_, err := jwt.ParseWithClaims(tplToken, &claims, func(token *jwt.Token) (interface{}, error) {
		return a.key, nil
	})
	if err != nil {
		return err
	}
	return a.svc.Send(ctx, claims.Tpl, args, number...)
}
