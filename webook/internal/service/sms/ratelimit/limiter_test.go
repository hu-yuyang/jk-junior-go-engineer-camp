package ratelimit

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/service/sms"
	smsmocks "JK-Junior-Go-Engineer-Camp/webook/internal/service/sms/mocks"
	"JK-Junior-Go-Engineer-Camp/webook/pkg/limiter"
	limitermocks "JK-Junior-Go-Engineer-Camp/webook/pkg/limiter/mocks"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"testing"
)

func TestRateLimitSMSService_Send(t *testing.T) {
	testCases := []struct {
		name string
		mock func(ctrl *gomock.Controller) (sms.Service, limiter.Limiter)
		// 预期输入
		// 因为我们要测试的这个方法的业务逻辑跟入参是没有关系的，他是直接把入参甩给 Send 的，
		// 所以我们可以一个输入都没有，在下面直接写死就可以

		// 预期输出
		wantErr error
	}{
		{
			name: "没有触发限流",
			mock: func(ctrl *gomock.Controller) (sms.Service, limiter.Limiter) {
				svc := smsmocks.NewMockService(ctrl)
				l := limitermocks.NewMockLimiter(ctrl)
				svc.EXPECT().Send(gomock.Any(), gomock.Any(),
					gomock.Any(), gomock.Any()).Return(nil)
				l.EXPECT().Limit(gomock.Any(), gomock.Any()).Return(false, nil)
				return svc, l
			},
		},
		{
			name: "触发限流",
			mock: func(ctrl *gomock.Controller) (sms.Service, limiter.Limiter) {

				l := limitermocks.NewMockLimiter(ctrl)
				svc := smsmocks.NewMockService(ctrl)
				l.EXPECT().Limit(gomock.Any(), gomock.Any()).Return(true, nil)
				// 限流了就不会执行发送了
				return svc, l
			},
			wantErr: errLimited,
		},
		{
			name: "限流器出现错误",
			mock: func(ctrl *gomock.Controller) (sms.Service, limiter.Limiter) {

				l := limitermocks.NewMockLimiter(ctrl)
				svc := smsmocks.NewMockService(ctrl)
				l.EXPECT().Limit(gomock.Any(), gomock.Any()).
					Return(false, errors.New("redis 限流器错误"))
				// 限流了就不会执行发送了
				return svc, l
			},
			wantErr: errors.New("redis 限流器错误"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			smsSvc, l := tc.mock(ctrl)
			rateLimitSMSService := NewRateLimitSMSService(smsSvc, l)
			err := rateLimitSMSService.Send(context.Background(),
				"abc", []string{"213"}, "123")
			assert.Equal(t, tc.wantErr, err)
		})
	}
}
