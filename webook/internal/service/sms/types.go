package sms

import "context"

// Service 发送短信的抽象
// 目前可以理解为，这是为了适配不同的短信供应商的抽象
// 为了屏蔽不同短信供应商之间的区别
type Service interface {
	// Send 发送短信
	// 目标手机号码、模板Id、模板参数
	Send(ctx context.Context, templateId string, args []string, number ...string) error
}
