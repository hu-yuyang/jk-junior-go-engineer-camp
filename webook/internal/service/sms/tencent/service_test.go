package tencent

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
	sms "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/sms/v20210111"
	"os"
	"testing"
)

func TestSender(t *testing.T) {
	// 这些都在环境变量中，需要手动设置
	secretId, ok := os.LookupEnv("SMS_SECRET_ID")
	if !ok {
		t.Fatal()
	}
	secretKey, ok := os.LookupEnv("SMS_SECRET_KEY")
	if !ok {
		t.Fatal()
	}
	client, err := sms.NewClient(common.NewCredential(secretId, secretKey), "ap-nanjing",
		profile.NewClientProfile())
	if err != nil {
		t.Fatal(err)
	}
	s := NewService(client, "xxx", "xxx")
	testCase := []struct {
		name    string
		tplId   string
		args    []string
		numbers []string
		wantErr error
	}{
		{
			name:    "发送验证码",
			tplId:   "1877556",
			args:    []string{"123456"},
			numbers: []string{"你的手机号"},
		},
	}
	for _, tc := range testCase {
		t.Run(tc.name, func(t *testing.T) {
			err = s.Send(context.Background(), tc.tplId, tc.args, tc.numbers...)
			assert.Equal(t, tc.wantErr, err)
		})
	}
}
