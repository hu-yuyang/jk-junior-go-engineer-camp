package service

import (
	"JK-Junior-Go-Engineer-Camp/webook/internal/domain"
	"JK-Junior-Go-Engineer-Camp/webook/internal/repository"
	repomocks "JK-Junior-Go-Engineer-Camp/webook/internal/repository/mocks"
	"JK-Junior-Go-Engineer-Camp/webook/pkg/logger"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"go.uber.org/mock/gomock"
	"testing"
)

func Test_articleService_Publish(t *testing.T) {
	testCases := []struct {
		name string
		// 模拟依赖
		mock func(ctrl *gomock.Controller) (repository.ArticleAuthorRepository,
			repository.ArticleReaderRepository)
		art domain.Article // 预期输入

		// 预期输出
		wantId  int64
		wantErr error
	}{
		{
			name: "新建帖子发表成功",
			mock: func(ctrl *gomock.Controller) (repository.ArticleAuthorRepository,
				repository.ArticleReaderRepository) {
				authorRepo := repomocks.NewMockArticleAuthorRepository(ctrl)
				authorRepo.EXPECT().Create(gomock.Any(), domain.Article{
					Title:   "标题",
					Content: "内容",
					Author: domain.Author{
						Id: 123,
					},
				}).Return(int64(1), nil)
				readerRepo := repomocks.NewMockArticleReaderRepository(ctrl)
				readerRepo.EXPECT().Save(gomock.Any(), domain.Article{
					Id:      1,
					Title:   "标题",
					Content: "内容",
					Author: domain.Author{
						Id: 123,
					},
				}).Return(nil)
				return authorRepo, readerRepo
			},
			art: domain.Article{
				Title:   "标题",
				Content: "内容",
				Author: domain.Author{
					Id: 123,
				},
			},
			wantId:  1,
			wantErr: nil,
		},
		{
			name: "修改并新发表成功",
			mock: func(ctrl *gomock.Controller) (repository.ArticleAuthorRepository,
				repository.ArticleReaderRepository) {
				authorRepo := repomocks.NewMockArticleAuthorRepository(ctrl)
				authorRepo.EXPECT().Update(gomock.Any(), domain.Article{
					Id:      1,
					Title:   "标题",
					Content: "内容",
					Author: domain.Author{
						Id: 123,
					},
				}).Return(nil)
				readerRepo := repomocks.NewMockArticleReaderRepository(ctrl)
				readerRepo.EXPECT().Save(gomock.Any(), domain.Article{
					Id:      1,
					Title:   "标题",
					Content: "内容",
					Author: domain.Author{
						Id: 123,
					},
				}).Return(nil)
				return authorRepo, readerRepo
			},
			art: domain.Article{
				Id:      1,
				Title:   "标题",
				Content: "内容",
				Author: domain.Author{
					Id: 123,
				},
			},
			wantId:  1,
			wantErr: nil,
		},
		{
			name: "修改但发表失败，重试成功",
			mock: func(ctrl *gomock.Controller) (repository.ArticleAuthorRepository,
				repository.ArticleReaderRepository) {
				authorRepo := repomocks.NewMockArticleAuthorRepository(ctrl)
				authorRepo.EXPECT().Update(gomock.Any(), domain.Article{
					Id:      1,
					Title:   "标题",
					Content: "内容",
					Author: domain.Author{
						Id: 123,
					},
				}).Return(nil)
				readerRepo := repomocks.NewMockArticleReaderRepository(ctrl)
				readerRepo.EXPECT().Save(gomock.Any(), domain.Article{
					Id:      1,
					Title:   "标题",
					Content: "内容",
					Author: domain.Author{
						Id: 123,
					},
				}).Return(errors.New("模拟数据库错误"))
				// 重试成功
				readerRepo.EXPECT().Save(gomock.Any(), domain.Article{
					Id:      1,
					Title:   "标题",
					Content: "内容",
					Author: domain.Author{
						Id: 123,
					},
				}).Return(nil)
				return authorRepo, readerRepo
			},
			art: domain.Article{
				Id:      1,
				Title:   "标题",
				Content: "内容",
				Author: domain.Author{
					Id: 123,
				},
			},
			wantId:  1,
			wantErr: nil,
		},
		{
			name: "修改但发表失败，重试失败",
			mock: func(ctrl *gomock.Controller) (repository.ArticleAuthorRepository,
				repository.ArticleReaderRepository) {
				authorRepo := repomocks.NewMockArticleAuthorRepository(ctrl)
				authorRepo.EXPECT().Update(gomock.Any(), domain.Article{
					Id:      1,
					Title:   "标题",
					Content: "内容",
					Author: domain.Author{
						Id: 123,
					},
				}).Return(nil)
				readerRepo := repomocks.NewMockArticleReaderRepository(ctrl)
				readerRepo.EXPECT().Save(gomock.Any(), domain.Article{
					Id:      1,
					Title:   "标题",
					Content: "内容",
					Author: domain.Author{
						Id: 123,
					},
				}).Times(3).Return(errors.New("模拟数据库错误"))
				return authorRepo, readerRepo
			},
			art: domain.Article{
				Id:      1,
				Title:   "标题",
				Content: "内容",
				Author: domain.Author{
					Id: 123,
				},
			},
			wantId:  1,
			wantErr: errors.New("保存到线上库失败，重试次数耗尽"),
		},
		{
			name: "保存到制作库失败",
			mock: func(ctrl *gomock.Controller) (repository.ArticleAuthorRepository,
				repository.ArticleReaderRepository) {
				authorRepo := repomocks.NewMockArticleAuthorRepository(ctrl)
				authorRepo.EXPECT().Update(gomock.Any(), domain.Article{
					Id:      1,
					Title:   "标题",
					Content: "内容",
					Author: domain.Author{
						Id: 123,
					},
				}).Return(errors.New("模拟数据库错误"))
				readerRepo := repomocks.NewMockArticleReaderRepository(ctrl)
				return authorRepo, readerRepo
			},
			art: domain.Article{
				Id:      1,
				Title:   "标题",
				Content: "内容",
				Author: domain.Author{
					Id: 123,
				},
			},
			wantErr: errors.New("模拟数据库错误"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// 1. 构造service
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			authorRepo, readerRepo := tc.mock(ctrl)
			svc := NewArticleServiceV1(readerRepo, authorRepo, logger.NewNopLogger())

			// 2. 执行测试
			id, err := svc.PublishV1(context.Background(), tc.art)
			assert.Equal(t, err, tc.wantErr)
			assert.Equal(t, id, tc.wantId)
		})
	}
}
