package domain

// WechatInfo 微信登录通过code获取access_token一步中，微信返回的结构中需要返回给前端的结构
type WechatInfo struct {
	// 授权用户唯一标识
	Openid string `json:"openid"`
	// 当且仅当该网站应用已获得该用户的userinfo授权时，才会出现该字段。
	UnionId string `json:"unionid"`
}
