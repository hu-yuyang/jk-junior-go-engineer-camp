package domain

type AsyncSms struct {
	Id       int64
	TplId    string   // 模板ID
	Args     []string // 短信内容
	Numbers  []string // 手机号
	RetryMax int      // 最大重试次数
}
