// Package logger 将各种类型封装成Field类型
package logger

var DEBUG = false

// Error error->Field
func Error(err error) Field {
	return Field{Key: "error", Val: err}
}

// String string->Field
func String(key string, val string) Field {
	return Field{Key: key, Val: val}
}

// Bool bool->Field
func Bool(key string, val bool) Field {
	return Field{Key: key, Val: val}
}

// Int32 int32->Field
func Int32(key string, val int32) Field {
	return Field{Key: key, Val: val}
}

// Int64 int64->Field
func Int64(key string, val int64) Field {
	return Field{Key: key, Val: val}
}

// Int int->Filed
func Int(key string, val int) Field {
	return Field{Key: key, Val: val}
}
